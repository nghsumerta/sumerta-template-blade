<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TanyaController extends Controller
{
    public function buatpertanyaan(){
      return view('tanya.form_tanya');
    }

    public function store (Request $request){
      //dd($request->all());
      $request->validate([
        'judul' => 'required|unique:pertanyaan',
        'isi' => 'required'
      ]);
      $query = DB::table('pertanyaan')->insert([
        "judul" => $request["judul"],
        "isi" => $request["isi"]
      ]);

      return redirect('/pertanyaan')->with('berhasil','Data berhasil disimpan');
    }

    public function index(){
      $tanya = DB::table('pertanyaan')->get();
      //dd($tanya); //$tanya = cuma query
      return view ('tanya.index', compact('tanya'));
    }

    public function tampil($id){
      $detail = DB::table('pertanyaan')->where('id', $id)->first();
      //dd($detail);
      return view ('tanya.tampil', compact('detail'));
    }

    public function edit($id){
      $detail = DB::table('pertanyaan')->where('id', $id)->first();

      return view('tanya.edit',compact('detail'));
    }

    public function update($id, Request $request){
      $request->validate([
        'judul' => 'required|unique:pertanyaan',
        'isi' => 'required'
      ]);
      $update = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                      'judul'=>$request['judul'],
                      'isi'=>$request['isi']
                    ]);

                    return redirect('/pertanyaan')->with('berhasil','Berhasil update data');
    }

    public function destroy ($id){
      $delete = DB::table('pertanyaan')->where('id',$id)->delete();
      return redirect ('/pertanyaan')->with('berhasil','Data Berhasil Dihapus');
    }


}
