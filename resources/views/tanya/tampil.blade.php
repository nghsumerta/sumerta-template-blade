@extends('layouts.master')

@section ('content')
<div class="container-fluid">

  <div class="card card-danger">
    <div class="card-header ">
      {{$detail->judul}}
    </div>
    <div class="card-body">
      <h5 class="card-title">Isi</h5>
      <p class="card-text">{{ $detail ->isi }}.</p>
    </div>
  </div>
</div>
@endsection
