@extends('layouts.master')

@section ('content')
<div class="ml-3 mt-2">
  <div class="card card-danger">
    <div class="card-header">
      <h3 class="card-title">Edit Pertanyaan {{$detail->id}}</h3>
    </div>
    <form role="form" action="/pertanyaan/ {{$detail->id}}" method="POST">
      @csrf
      @method('PUT')
      <div class="card-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Judul Pertanyaan</label>
          <input type="text" class="form-control" name="judul" value="{{old('judul',$detail->judul)}}" id="judul" placeholder="Judul pertanyaan">
            @error('judul')
            <div class="alert alert-danger"> {{$message}}</div>
            @enderror
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Isi Pertanyaan</label>
          <input type="textarea" class="form-control" name="isi" value="{{old('isi',$detail->isi)}}" id="isi" placeholder="Isi pertanyaan">
          @error('isi')
          <div class="alert alert-danger"> {{$message}}</div>
          @enderror

        </div>
      </div>
      </div>

      <div class="card-footer">
        <button type="submit" class="btn btn-danger">Update Pertanyaan</button>
      </div>
    </form>
  </div>

</div>



@endsection
