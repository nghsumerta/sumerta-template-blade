@extends('layouts.master')

@section ('content')
  <div class="mt-3 ml-4">
    <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Daftar Pertanyaan</h3>
    </div>
    <div class="card-body">
      @if(session('berhasil'))
        <div class="alert alert-success">
          {{ session('berhasil') }}
        </div>
      @endif
      <a href="/pertanyaan/create"class="btn btn-info mb-2">Buat Pertanyaan Baru</a>
    <table class="table">
      <thead class="thead-dark">
        <tr>
          <th scope="col">No</th>
          <th scope="col">Judul</th>
          <th scope="col">Isi</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        @forelse($tanya as $key => $tanya1)
          <tr>
            <td> {{$key + 1}} </td>
            <td> {{$tanya1-> judul}} </td>
            <td> {{$tanya1-> isi}} </td>
            <td>
              <a href="/pertanyaan/{{$tanya1->id}}" class="btn btn-info btn-sm">Detail</a>
              <a href="/pertanyaan/{{$tanya1->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              <form class="" action="/pertanyaan/{{$tanya1->id}}" method="post">
                @csrf
                @method('DELETE')
                <input type="submit"  value="delete" class="btn btn-danger btn-sm">
              </form>
            </td>
          </tr>
          @empty
          <tr>
            <td colspan="4" align="center">No Post</td>
          </tr>

        @endforelse
        <!-- $tanya1 adalah variable baru -->
      </tbody>
    </table>
  </div>
  </div>

  </div>

@endsection
