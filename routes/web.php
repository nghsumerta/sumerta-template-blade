<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'AdminController@index');
Route::get('/data-table', 'AdminController@table');
Route::get('/dashboard', 'AdminController@dashboard');

Route::get('/pertanyaan/create', 'TanyaController@buatpertanyaan');
Route::post('/pertanyaan','TanyaController@store');
Route::get('/pertanyaan','TanyaController@index');
Route::get('/pertanyaan/{id}', 'TanyaController@tampil');
Route::get('/pertanyaan/{id}/edit', 'TanyaController@edit');
Route::put('/pertanyaan/{id}', 'TanyaController@update');
Route::delete('/pertanyaan/{id}','TanyaController@destroy');
